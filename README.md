# Crypto Rates Backend

Fetch, Transform, Store and Send crypto-currencies rates data from multiple external public APIs with a simple yet scalable architecture.

## Setup

### mLab
Create a Cluster on mLab

### Install NPM Packages
`npm i`

### Set Env Variables
- Open `.env-default`
- Set your DBaaS MongoDB variables
- Rename `.env-default` into `.env` 

### Run 
`node api.js`
