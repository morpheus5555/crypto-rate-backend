const request = require('request');
const Promise = require('bluebird');
const { db, CurrencyModel } = require('./db');

const Fetcher = function (url) {
  return {
    fetch: () => new Promise(function (resolve, reject) {
      console.info(`Fetching ${url}`)
      request
        .get(url, (err, resp, body) => {
					if(err) {
						reject();
					} else {
						resolve(body);
					}
        })
    }),
  }
}

const Transformer = function (transform) {
  return { transform: transform }
}

/*
 * Handy and Simple API Object representation that encapsulate everything we need
 * such as a composition of a Fetcher and a Transformer. 
 * It also permit a quick and epurated way of creating API interfaces.
 */
const API = function (url,  transform) {
  const api_name = url.split('://')[1].split('/')[0]

  return Object.assign({
      url,
      api_name
    },
    Fetcher(url),
    Transformer(transform)
  )
}

// The API list
const APIS = [
  API("https://wallet-api.celsius.network/util/interest/rates", function(data) {
    return JSON.parse(data)["interestRates"].map((curr,index) => {
      return {
        api: this.api_name,
        url: this.url,
        rate: (curr.rate * 100).toFixed(2),
        symbol: curr.coin,
        name: curr.currency.name,
        date: Date.now(),
      }
    })
  }),
  API("https://api.compound.finance/api/v2/ctoken", function (data) { 
    return  JSON.parse(data)["cToken"].map((curr,index) => {
      return {
        api: this.api_name,
        url: this.url,
        rate: (curr.borrow_rate.value * 100).toFixed(2),
        symbol: curr.underlying_symbol,
        name: curr.underlying_name,
        date: Date.now(),
      }
    })
  }),
]

exports.Command = function () {

  // Fetch datas from all APIs
  const fetch_all = () => 
    Promise.map(APIS, function(API) { 
			return API
				.fetch()
				.then(API.transform.bind(API))
				.catch(err => {
					console.log(err)
				})
		})

  // Post process all fetched api datas as a whole
  const post_process = (API_datas) => new Promise(function(resolve, reject) {
    // Flatten the arrays into one
    API_datas = [].concat.apply([], API_datas).filter(data => data !== undefined )

		get_current_tick().then(tick => {
			API_datas.map(API => API._tick = tick+1)
			resolve(API_datas)
		})
		.catch(err => {
			console.log(err)
		})
  })

  const update_all = (API_datas) => new Promise(function(resolve, reject) {
    CurrencyModel
      .insertMany(API_datas)
      // Debug
      /*.then(function(currency_records) {
        console.info(currency_records)
      })
      .catch(err => console.error(err))*/
  })

	const get_current_tick = (api) => new Promise(function(resolve, reject) {
		let filter = api === undefined? {} : { api: api }
		return CurrencyModel
			.findOne(filter)
			.sort({date: -1})
			.exec(function(err, api) {
				if(api) tick = api._tick
				else 		tick = 0
				resolve(tick)
				return tick;
			})
		})

  return {
		get_all_api_names: function () {
			return APIS.map(API => API.api_name)
		},

		get_current_tick,

    // Execute one full Fetch->Transform->Post-Prod->Update cycle
    tick: function () {
        fetch_all()
          .then(post_process)
          .then(update_all)
    },
  }
}
