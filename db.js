const mongoose = require('mongoose');

// Mongoose Schema of a Currency rates record
const CurrencySchema = new mongoose.Schema({ 
  api: String,
  url: String,
  rate: Number,
  symbol: String,
  name: String,
  date: { type: Date, default: Date.now },
  _tick: Number,
}, { collection: 'currencies' });

exports.CurrencyModel = mongoose.model('Currency', CurrencySchema);

// ---

// MongoDB Connection
mongoose.connect(`mongodb+srv://${ process.env.DB_USERNAME }:${ process.env.DB_PASSWORD }@${ process.env.DB_ADDR }/${ process.env.DB_NAME }?retryWrites=true&w=majority`,
  {	useNewUrlParser: true, useUnifiedTopology: true });

exports.db = mongoose.connection;
exports.db.on('error', console.error.bind(console, 'connection error:'));

