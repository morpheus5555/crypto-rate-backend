require('dotenv').config()
const CronJob = require('cron').CronJob;
const Promise = require('bluebird');
const express = require('express');
const { Command } = require('./external_api');
const command = Command();
const { db, CurrencyModel } = require('./db')
const app = express()
const port = 3000

function sendJSON(res,data) {
  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify(data));
}

/*
 *	Get all last retrieved datas of all apis
 */
app.get('/api/all', (req, res) => {
	const requests = command.get_all_api_names()
		.map(api_name => {
			return new Promise (function(resolve, reject) {
				command
					.get_current_tick(api_name)
					.then(tick => {
						return CurrencyModel
							.find({
								'api': api_name,
								'_tick': tick,
							})
							.sort({date: 'desc'})
							.exec((err, currencies) => {
								if(err) throw(err)
								resolve(currencies)
							})
					})
			})
		})
	
	Promise.all(requests).then(function(datas) {
    datas = [].concat.apply([], datas) 
		sendJSON(res,datas)
	})
})

// WARNING: Not Secure.
// Must add a token system or an IP restriction in the future.
app.delete('/api/all', (req, res) => {
  return CurrencyModel
    .deleteMany({})
    .exec((err,currencies) => {
      if(err) throw(err)
      sendJSON(res,{message: "Database has been Purged."})
    })	
})

// Suited for a Home page by example
/*app.get('/api/preview', function (req, res) {
  return CurrencyModel.find({})
    .sort({date: 'desc'})
    .exec(function(err, currencies) {
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(currencies))
    });
})
*/

app.get(['/api/:symbol/', '/api/:symbol/:limit'], (req, res) => {
  return CurrencyModel
    .find({symbol: req.params.symbol})
    .sort({date: 'desc'})
    .limit(req.params.limit || 200)
    .exec((err, currencies) => {
      if(err) throw(err)
      sendJSON(res,currencies)
    })
})


db.once('open', function() {

  console.info('Connection to MongoDB: Success')

  // One tick on load for development
  // command.tick()
  
  const job = new CronJob('0 */30 * * * *', command.tick);
  job.start();
});

app.listen(port, () => console.log(`Listening on port ${port}!`))
